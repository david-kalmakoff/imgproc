package main

import (
	"image"
	"log"
	"strings"
	"sync"

	"github.com/disintegration/imaging"
)

func minDimension(src image.Image) int {
	w := src.Bounds().Dx()
	h := src.Bounds().Dy()

	if w < h {
		return w
	}

	return h
}

type imageFormat struct {
	name  string
	width int
}

var sizes = []imageFormat{{
	name:  "lg",
	width: 1080,
}, {
	name:  "md",
	width: 720,
}, {
	name:  "sm",
	width: 500,
}, {
	name:  "xs",
	width: 150,
}}

type imageDef struct {
	image image.Image
	name  string
}

type imageInput struct {
	filename string
	orig     imageDef
	crop     imageDef
	fileType string
}

func readImage(fn string, crop bool, name string) imageInput {
	var img imageInput
	img.filename = fn

	slc := strings.Split(fn, ".")
	if len(slc) != 2 {
		log.Fatal("failed to read image")
	}

	img.fileType = strings.ToLower(slc[1])

	if name != "" {
		img.orig.name = name
	} else {
		img.orig.name = strings.ToLower(slc[0])
	}

	var err error
	img.orig.image, err = imaging.Open(fn)
	if err != nil {
		log.Fatalf("failed to open image: %v", err)
	}

	if crop {
		if name != "" {
			img.crop.name = name + "-crop"
		} else {
			img.crop.name = strings.ToLower(slc[0]) + "-crop"
		}
		minDim := minDimension(img.orig.image)
		img.crop.image = imaging.CropAnchor(img.orig.image, minDim, minDim, imaging.Center)
	}

	return img
}

type imageNew struct {
	filename        string
	image           image.Image
	croppedFilename string
	croppedImage    image.Image
}

func createNewImage(old imageInput, size imageFormat, crop bool) imageNew {
	var img imageNew
	img.image = imaging.Resize(old.orig.image, size.width, 0, imaging.Lanczos)

	postfix := "-" + size.name + "." + old.fileType
	img.filename = old.orig.name + postfix
	if crop {
		img.croppedImage = imaging.Resize(old.crop.image, size.width, 0, imaging.Lanczos)
		img.croppedFilename = old.crop.name + postfix
	}

	return img
}

func (img imageNew) save(output string, crop bool) {
	if output != "" {
		if string(output[len(output)-1]) != "/" {
			output = output + "/"
		}
		img.filename = output + img.filename
		img.croppedFilename = output + img.croppedFilename
	}

	var wg sync.WaitGroup
	wg.Add(2)

	go func() {
		defer wg.Done()

		err := imaging.Save(img.image, img.filename, imaging.JPEGQuality(75))
		if err != nil {
			log.Fatalf("failed to save image: %v", err)
		}
	}()

	if !crop {
		wg.Done()
	} else {
		go func() {
			defer wg.Done()

			err := imaging.Save(img.croppedImage, img.croppedFilename, imaging.JPEGQuality(75))
			if err != nil {
				log.Fatalf("failed to save image: %v", err)
			}
		}()
	}

	wg.Wait()
}

func convertImage(file string, name string, crop bool, output string) {
	imgConv := readImage(file, crop, name)

	var wg sync.WaitGroup
	wg.Add(len(sizes))

	for _, size := range sizes {
		go func(size imageFormat) {
			defer wg.Done()

			img := createNewImage(imgConv, size, crop)

			img.save(output, crop)
		}(size)
	}

	wg.Wait()
}
