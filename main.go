package main

import (
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/urfave/cli/v2"
)

func main() {
	app := &cli.App{
		Name:     "imgproc",
		Usage:    "process images for web use",
		Commands: []*cli.Command{
			// {
			// 	Name:    "favicon",
			// 	Aliases: []string{"f"},
			// 	Usage:   "create favicon version of image",
			// 	Action: func(cCtx *cli.Context) error {
			// 		fmt.Println("added task: ", cCtx.Args().First())
			// 		return nil
			// 	},
			// },
		},
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:    "name",
				Usage:   "name of file output, default uses current file name",
				Aliases: []string{"n"},
			},
			&cli.StringFlag{
				Name:    "output",
				Usage:   "destination of file output, default creates files in current directory",
				Aliases: []string{"o"},
			},
			&cli.BoolFlag{
				Name:    "crop",
				Usage:   "option to created cropped version of image",
				Aliases: []string{"c"},
			},
		},
		Action: func(c *cli.Context) error {
			pattern := c.Args().Slice()
			// TODO: check to see if it is proper filetype jpg, jpeg, png
			// filetypes := []string{"jpg", "jpeg", "png"}
			for _, file := range pattern {
				slc := strings.Split(file, ".")
				filetype := strings.ToLower(slc[len(slc)-1])
				if filetype != "jpg" {
					return fmt.Errorf("invalid file type")
				}
			}

			for _, file := range pattern {
				// TODO: use name, output and crop in image conversion
				convertImage(file, c.String("name"), c.Bool("crop"), c.String("output"))
			}

			fmt.Printf("converted %d image(s)\n", len(pattern))
			return nil
		},
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

/*
TODO:
- handle jpg + png
- create CLI
- allow for multiple files (*.jpg, *.png, *)
- take -name flag to replace name
*/
